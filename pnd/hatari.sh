#!/bin/sh

export LD_LIBRARY_PATH="$PWD/lib:$LD_LIBRARY_PATH"
export HOME=$PWD XDG_CONFIG_HOME=$PWD

if [ ! -e $HOME/.config/hatari/hatari.cfg ]
then
	mkdir -p $HOME/.config/hatari
	cp $PWD/hatari.cfg $HOME/.config/hatari/
fi

if [ ! -e firstrun ]
then
	touch firstrun
	if [ ! -e tos.img ]
	then
		zenity --question --title "EmuTOS installer" --text "No tos.img file was found in appdata/hatari-sl! Would you like to try EmuTOS ?"
		if [ $? = 0 ]
		then
			unzip emutos-512k-*.zip
			cp emutos-512k-*/etos512k.img tos.img
		fi
	fi
fi

if [ -n "$1" ] && [ -z "$2" ]
then
	# If there is only one argument, try to identify file type
	# and run hatari with parameter for guessed type.
	arg=$1

	if echo "$arg"|grep -qi "\.\(cfg\)$"
	then
		./hatari --configfile "$arg"

	elif echo "$arg"|grep -qi "\.\(st\|msa\|zip\)$"
	then
		./hatari --disk-a "$arg"

	elif echo "$arg"|grep -qi "\.\(acsi\|img\)$"
	then
		./hatari --acsi "$arg"

	elif echo "$arg"|grep -qi "\.\(ide\)$"
	then
		./hatari --ide-master "$arg"
	else
		./hatari "$@"
	fi
else
	./hatari "$@"
fi

exitcode=$?

sudo -n /usr/pandora/scripts/op_lcdrate.sh 60

exit $exitcode
