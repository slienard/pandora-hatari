# Pandora-Hatari

## Description

Open Pandora handheld version of the Hatari Atari ST emulator, that can use the [OMAP SDL video driver](https://github.com/notaz/sdl_omap/) to get:
* Double buffering
* Vsync
* Hardware scaling

## Notes
* When using OMAP SDL video driver:
  * Hardware scaling is enabled only in ST Low resolution (320x200)
  * F11 is used to change hardware scaling modes (instead of switching windowed/fullscreen modes)
  * You should set "Frame skip = 0" to get best results with vsync if game/demo runs at 50 FPS
  * It's fullscreen only
* Like [Magicsam port](https://repo.openpandora.org/?page=detail&app=hatari-magicsam), this package contains EmuTOS
* Actually, With OMAP SDL video driver and ST low/medium resolutions, vsync is always enabled

## Known issues
* When using OMAP SDL video driver:
  * Small glitches with cursor in configuration UI
  * In emulated system, sometimes, mouse cannot be moved in some direction. You need to move it to the opposite border, then move it where you initially wanted to go. (This seems to come from the OMAP SDL video driver not supporting mouse relative mode).
* In emulated system, touch screen events are not at the right coordinates.

## Package
You can found the package on Pandora's repository: https://repo.openpandora.org/?page=detail&app=hatari.slix
