* v2.1.0.slix3
  * [Scaling] Also scale in fullscreen when max width and max height give a zoom factor of 1 and borders are enabled.
  * [PND] Also handle configuration files as single script argument
  * [PND] Exit with Hatari's exit code

* v2.1.0.slix2
  * [Vsync] Detect 50/60Hz switches and set LCD refresh rate accordingly
  * [UI] Fix file selector edit field (for example when saving config, it was not correctly updated when editing)
  * [PND] Add files associations (not perfect, PXML doc seems not up to date on how to do it best)
  * [PND] Allow to directly pass files as arguments (to ease file associations, but can be used in command-line with "pnd_run.sh")

* v2.1.0.slix1
  * Initial release
